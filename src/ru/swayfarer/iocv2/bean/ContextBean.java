package ru.swayfarer.iocv2.bean;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import ru.swayfarer.iocv2.bean.params.BeanParams;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction0;
import ru.swayfarer.swl2.markers.InternalElement;

/**
 * Элемент контекста <br> 
 * Все бины контекста представляют собой реализацию этого класса. <br> 
 * Содержит конфигурацию элемента контекста и функцию, возвращающие новые значения <br>
 * @author swayfarer
 *
 */
@Data
@Builder
@SuppressWarnings("unchecked")
public class ContextBean {

	/**
	 * Параметры бина <br> 
	 * Содержат настройки этого бина.
	 */
	@InternalElement
	@NonNull
	public BeanParams beanParams;
	
	/**
	 * Функция, возвращающая новые значения бина 
	 */
	@InternalElement
	@NonNull
	public IFunction0<Object> valueCreationFun;
	
	/**
	 * Получить значение бина <br> 
	 * В зависимости от логики и типа бина будет возвращать значения, которые будут подставляться при иньекциях элементов контекста.
	 * @return Значение бина 
	 */
	public <T> T getValue() {
		return (T) valueCreationFun.apply();
	}

}
