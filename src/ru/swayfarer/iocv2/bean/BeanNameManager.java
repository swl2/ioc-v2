package ru.swayfarer.iocv2.bean;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import ru.swayfarer.iocv2.annotations.ForName;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1;
import ru.swayfarer.swl2.markers.InternalElement;
import ru.swayfarer.swl2.string.StringUtils;

/**
 * Служебный класс, отвечаюший за получение имен бинов из самых разных мест <br> 
 * Содержит разные методы, получающие имена бинов <br> 
 * Когда они все в одном месте, их логику проще менять <br> 
 * @author swayfarer
 *
 */
public class BeanNameManager {

	/**
	 * Зарегистрированные функции, возвращающие имена бинов, получаемых из методов в источниках контекста <br> 
	 * Вызываются по очереди, пока хотя бы один не вернет не null.  <br> 
	 * Сюда можно добавлять кастомную логику =)
	 */
	@InternalElement
	public IExtendedList<IFunction1<Method, String>> methodElementNameFuns = CollectionsSWL.createExtendedList();
	
	/**
	 * Зарегистрированные функции, возвращающие имена бинов, получаемых для полей при иньекции <br> 
	 * Вызываются по очереди, пока хотя бы один не вернет не null.  <br> 
	 * Сюда можно добавлять кастомную логику =)
	 */
	@InternalElement
	public IExtendedList<IFunction1<Field, String>> fieldElementNameFuns = CollectionsSWL.createExtendedList();
	
	/**
	 * Зарегистрированные функции, возвращающие имена бинов, получаемых классов при сканировании бинов-компонентов <br> 
	 * Вызываются по очереди, пока хотя бы один не вернет не null.  <br> 
	 * Сюда можно добавлять кастомную логику =)
	 */
	@InternalElement
	public IExtendedList<IFunction1<Class<?>, String>> classElementNameFuns = CollectionsSWL.createExtendedList();
	
	/**
	 * Получить имя бина, который будет получен на основании класса. 
	 * @param cl Класс
	 * @return Имя бина 
	 */
	public String getBeanName(Class<?> cl)
	{
		for (var fun : classElementNameFuns)
		{
			var ret = fun.apply(cl);
			
			if (ret != null)
				return ret;
		}
		
		return null;
	}
	
	/**
	 * Получить имя бина, который надо заиньектить в поле
	 * @param field Поле
	 * @return Имя бина 
	 */
	public String getBeanName(Field field)
	{
		for (var fun : fieldElementNameFuns)
		{
			var ret = fun.apply(field);
			
			if (ret != null)
				return ret;
		}
		
		return null;
	}
	
	/**
	 * Получить имя бина, который будет получен из метода в источниках контекста
	 * @param method Метод-источник
	 * @return Имя
	 */
	public String getBeanName(Method method)
	{
		for (var fun : methodElementNameFuns)
		{
			var ret = fun.apply(method);
			
			if (ret != null)
				return ret;
		}
		
		return null;
	}
	
	/**
	 * Зарегистрировать стандартные функции для получения имен бинов <br> 
	 * Логика по-умолчанию, чтобы каждый раз не регать 
	 */
	public void registerDefaultFuns()
	{
		classElementNameFuns.add(ForName.classToBeanNameFun);
		classElementNameFuns.add((cl) -> StringUtils.firstCharToLowerCase(cl.getSimpleName()));
		
		fieldElementNameFuns.add(ForName.fieldToBeanNameFun);
		fieldElementNameFuns.add((field) -> field.getName());
		
		methodElementNameFuns.add(ForName.methodToBeanNameFun);
		methodElementNameFuns.add((method) -> {
			
			var methodName = method.getName();
			
			if (methodName.startsWith("get") && methodName.length() > 3)
			{
				methodName = StringUtils.firstCharToLowerCase(methodName.substring(3));
			}
			
			return methodName;
		});
	}
}
