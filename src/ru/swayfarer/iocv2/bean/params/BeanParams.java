package ru.swayfarer.iocv2.bean.params;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv2.annotations.LazyBean;
import ru.swayfarer.iocv2.bean.BeanType;
import ru.swayfarer.iocv2.context.Context;

/**
 * Конфигурация бина  <br> 
 * Содержит настройки бина, такие как тип, имя и пр. <br> 
 * На основании этих настроек формируется логика бина <br> 
 * @author swayfarer
 *
 */
@Data
@Builder
@NoArgsConstructor @AllArgsConstructor
@Accessors(chain = true)
public class BeanParams {

	/** Контекст, в котором находится бин */
	@ToString.Exclude
	public Context context;
	
	/** Имя бина */
	public String name;
	
	/** 
	 * Java-Тип, с которым ассоциируется бин <br> 
	 * По этому значению находятся бины в контексте, даже если конечное значение бина по типу будет отличаться <br>
	 * Например, тип может быть интерфейсом 
	 */
	public Class<?> type;
	
	/** Синглтон ли? (см {@link BeanType#Singleton}) */
	public boolean isSingleton;
	
	/** Ленивый ли бин? (см {@link LazyBean}) */
	public boolean isLazy;
	
	/** Тип элемента контекста */
	public BeanType beanType;
	
}
