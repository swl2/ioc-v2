package ru.swayfarer.iocv2.componentscan;

import ru.swayfarer.iocv2.annotations.EnableAutoScan;
import ru.swayfarer.iocv2.context.Contexts;
import ru.swayfarer.swl2.classes.ReflectionUtils;
import ru.swayfarer.swl2.collections.streams.DataStream;
import ru.swayfarer.swl2.exceptions.ExceptionsUtils;
import ru.swayfarer.swl2.string.StringUtils;

/**
 * Автоматический сканнер составляющих контекста, таких, как источники или компоненты <br> 
 * Сканирование настраивается аннотацией {@link EnableAutoScan}
 * @author swayfarer
 *
 */
public class AutoComponentScan {

	/**
	 * Сканировать пакет, из которого вызван метод <br>
	 * Добавит составляющие контекста, найденные в этом пакете. 
	 */
	public static void scanThis()
	{
		scan(ReflectionUtils.findClass(ExceptionsUtils.getCallerStacktrace().getClassName()));
	}
	
	/**
	 * Сканировать класс объекта и заиньектить зависимости в него.
	 * @param obj Сканируемый объект
	 */
	public static void scan(Object obj)
	{
		scan(obj.getClass());
		Contexts.injectContextElements(obj);
	}
	
	/**
	 * Сканировать на основании аннотации над указанным классом
	 * @param cl Класс, над которым находится аннотация {@link EnableAutoScan} 
	 */
	public static void scan(Class<?> cl)
	{
		EnableAutoScan autoScanAnnotation = ReflectionUtils.findAnnotationRec(cl, EnableAutoScan.class);
		
		ComponentScan componentScan = new ComponentScan();
		
		if (autoScanAnnotation != null)
		{
			var contextName = autoScanAnnotation.context();
			
			if (!StringUtils.isBlank(contextName))
				Contexts.context(contextName)
					.pattern(cl.getPackage().getName())
					.pattern(autoScanAnnotation.packages())
				;
			
			if (autoScanAnnotation.sources())
				componentScan.classFinder.eventScan.subscribe(new SourcesScan());
			
			if (autoScanAnnotation.beans())
			{
				var beansScan = new BeanScan().registerDefaultCreationFuns();
				componentScan.classFinder.eventScan.subscribe(beansScan);
			}
			
			DataStream.of(autoScanAnnotation.packages())
				.map((s) -> s.isEmpty() ? cl.getPackage().getName() : s)
				.each((s) -> componentScan.scan(s));
		}
	}
}
