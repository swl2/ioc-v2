package ru.swayfarer.iocv2.componentscan;

import javax.annotation.PostConstruct;

import ru.swayfarer.iocv2.annotations.Bean;
import ru.swayfarer.iocv2.annotations.Injection;
import ru.swayfarer.iocv2.annotations.LazyBean;
import ru.swayfarer.iocv2.bean.BeanType;
import ru.swayfarer.iocv2.bean.ContextBean;
import ru.swayfarer.iocv2.bean.params.BeanParams;
import ru.swayfarer.iocv2.context.Contexts;
import ru.swayfarer.iocv2.exception.IoCException;
import ru.swayfarer.swl2.asm.AsmUtils;
import ru.swayfarer.swl2.asm.informated.ClassInfo;
import ru.swayfarer.swl2.classes.ReflectionUtils;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.exception.ExceptionHandler;
import ru.swayfarer.swl2.exceptions.ExceptionsUtils;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1NoR;
import ru.swayfarer.swl2.markers.InternalElement;
import ru.swayfarer.swl2.z.dependencies.org.objectweb.asm.Type;

/**
 * Сканнер бинов <br> 
 * Часть сканирования компонентов, которая ищет классы, отмеченные аннотацией {@link Bean} <br> 
 * Найденные классы будут обработаны как компоненты: на их основе будут созданы бины, которые потом будут добавлены в контекст. <br>
 * 
 * @author swayfarer
 *
 */
@SuppressWarnings("unchecked")
public class BeanScan implements IFunction1NoR<ClassInfo> {

	/** 
	 * Обработчик ошибок <br> 
	 * Настрой его под себя =)
	 */
	public ExceptionHandler exceptionHandler = new ExceptionHandler();
	
	/**
	 * Функции, создающие инстанс бина из класса. <br>
	 * Вызываются по очереди, пока хотя бы один не вернет не null.  <br> 
	 * Сюда можно добавлять кастомную логику =)
	 */
	public IExtendedList<IFunction1<Class<?>, Object>> instanceCreationFuns = CollectionsSWL.createExtendedList();
	
	/** Дескриптор аннотации {@link Bean}, который будет искаться при сканировании компонентов */
	@InternalElement
	public static String beanAnnotationDesc = Type.getDescriptor(Bean.class);
	
	/** 
	 * Просканировать класс на уровне байткода
	 * @param classInfo Информация о классе, полученная при сканировании его байткода 
	 */
	public void scan(ClassInfo classInfo)
	{
		var annotation = AsmUtils.findAnnotationRec(classInfo, beanAnnotationDesc);
		
		if (annotation != null) 
		{
			var componentClass = ReflectionUtils.findClass(classInfo.getCanonicalName());
			ExceptionsUtils.IfNull(componentClass, IoCException.class, "Can't create bean of class", classInfo.getCanonicalName(), "because it class can't be loaded!");
			scan(componentClass);
		}
	}
	
	/**
	 * Просканировать класс через рефлексию <br>
	 * @param componentClass Сканируемый класс 
	 */
	public void scan(Class<?> componentClass)
	{
		LazyBean lazyBeanAnnotation = ReflectionUtils.findAnnotationRec(componentClass, LazyBean.class);
		Bean beanAnnotation = ReflectionUtils.findAnnotationRec(componentClass, Bean.class);
		
		var componentContext = Contexts.findOrCreateContext(componentClass);
		
		var contextBean = ContextBean.builder()
				.beanParams(BeanParams.builder()
						.name(componentContext.getConfiguration().getBeanNameManager().getBeanName(componentClass))
						.beanType(beanAnnotation.type())
						.context(componentContext)
						.isSingleton(beanAnnotation.type() == BeanType.Singleton)
						.isLazy(lazyBeanAnnotation == null ? true : lazyBeanAnnotation.value())
						.type(componentClass)
						.build())
				.valueCreationFun(() -> newInstance(componentClass))
		.build();
		
		ExceptionsUtils.IfNull(componentContext, IoCException.class, "Can't find registered context for", componentClass, "Maybe it's not registered?");
		
		componentContext.addBean(contextBean);
	}
	
	/**
	 * Создать новый экземпляр бина <br> 
	 * Новый экземпляр создается для каждого нового значения бина <br>
	 * @param cl Класс, на основе которого создается новый экземпляр
	 * @return Экземпляр или null
	 */
	@InternalElement
	public Object newInstance(Class<?> cl)
	{
		for (var fun : instanceCreationFuns)
		{
			var ret = fun.apply(cl);
			
			if (ret != null)
			{
				Contexts.injectContextElements(ret);
				
				ReflectionUtils.methods(ret)
					.withoutArgs()
					.annotated(PostConstruct.class)
				.invoke(ret);
				
				return ret;
			}
		}
		
		return null;
	}
	
	/**
	 * Зарегистрировать стандартную логику создания значений бинов по классу <br>
	 * см. {@link #instanceCreationFuns}
	 * @return this
	 */
	public <T extends BeanScan> T registerDefaultCreationFuns()
	{
		// Создание через констуктор без агрументов 
		instanceCreationFuns.add((cl) -> {
			
			var defaultConstructor = ReflectionUtils.constructors(cl)
				.publics()
				.withoutArgs()
				.first();
			
			if (defaultConstructor != null)
			{
				return exceptionHandler.safeReturn(() -> defaultConstructor.newInstance(), null, "Error while creating new instance of", cl, "by default constructor");
			}
			
			return null;
		});
		
		// Создание через метод, отмеченный @Injection
		instanceCreationFuns.add((cl) -> {
			
			var annotatedConstructors = ReflectionUtils.constructors(cl)
				.publics()
				.filter((c) -> ReflectionUtils.findAnnotationRec(c, Injection.class) != null);
			
			ExceptionsUtils.If(annotatedConstructors.size() > 1, IoCException.class, "More than one", Injection.class, "annotated constructors for", cl, "It's not allowed!");
			
			var constructor = annotatedConstructors.first();
			var context = Contexts.findContext(cl);
			var args = context.getContextManager().findParamsFromContext(constructor, constructor.getParameters());
			
			return exceptionHandler.safeReturn(() -> constructor.newInstance(args.toArray()), null, "Error while creating new instance of", cl, "by", constructor);
		});
		
		// Создание через конструктор с аргументами, подобранными из контекста.
		instanceCreationFuns.add((cl) -> {
			
			var annotatedConstructors = ReflectionUtils.constructors(cl)
				.publics();
			
			ExceptionsUtils.If(annotatedConstructors.size() > 1, IoCException.class, "More than one constructors for", cl, "It's not allowed!");
			
			var constructor = annotatedConstructors.first();
			var context = Contexts.findContext(cl);
			var args = context.getContextManager().findParamsFromContext(constructor, constructor.getParameters());
			
			return exceptionHandler.safeReturn(() -> constructor.newInstance(args.toArray()), null, "Error while creating new instance of", cl, "by", constructor);
		});
		
		return (T) this;
	}

	@Override
	public void applyNoR(ClassInfo classInfo) 
	{
		scan(classInfo);
	}
}
