package ru.swayfarer.iocv2.componentscan;

import java.util.concurrent.atomic.AtomicInteger;

import ru.swayfarer.iocv2.exception.IoCException;
import ru.swayfarer.loggingv2.LoggerV2;
import ru.swayfarer.loggingv2.manager.LoggingManagerV2;
import ru.swayfarer.swl2.asm.classfinder.ClassFinder;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.exception.ExceptionHandler;
import ru.swayfarer.swl2.exceptions.ExceptionsUtils;
import ru.swayfarer.swl2.markers.InternalElement;

/**
 * Сканнер составляющих контекстов внутри classpath'а <br> 
 * Позволяет вешать на себя кастомную логику через подписку на {@link #classFinder} <br>
 * 
 * @author swayfarer
 *
 */
public class ComponentScan {
	
	public LoggerV2 logger = LoggingManagerV2.getLogger();
	
	public ExceptionHandler exceptionHandler = new ExceptionHandler()
			.configure()
				.rule()
					.log()
	.toHandler();
	
	/**
	 * Список пакетов, которые уже были просканированы. <br> 
	 * Позволяет не сканировать пакеты дважды 
	 */
	public IExtendedList<String> alreadyScanned = CollectionsSWL.createExtendedList();
	
	/** Количество сканирований */
	public AtomicInteger scansCount = new AtomicInteger();
	
	/** Искалка классов, на основании данных которых работает сканирование IoC*/
	public ClassFinder classFinder = new ClassFinder();
	
	/**
	 * Просканировать пакет
	 * @param pkg Сканируемый пакет через точки
	 */
	public synchronized void scan(String pkg)
	{
		exceptionHandler.safe(() -> {
			
			ExceptionsUtils.IfNot(checkNoScan(pkg), IoCException.class, "Package '", pkg, "' is already scanned!");
			
			alreadyScanned.add(pkg);
			classFinder.scan(pkg);
			scansCount.incrementAndGet();
			
		}, "Error while scanning package", pkg);
	}
	
	/**
	 * Убедиться, что пакет не был просканирован  
	 * @param scan Имя пакета
	 * @return True, если пакет не был просканирован
	 */
	@InternalElement
	public boolean checkNoScan(String scan)
	{
		return alreadyScanned.find((scannedPkg) -> scan.startsWith(scannedPkg) || scannedPkg.startsWith(scan)) == null;
	}
	
}
