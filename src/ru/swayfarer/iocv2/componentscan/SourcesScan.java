package ru.swayfarer.iocv2.componentscan;

import ru.swayfarer.iocv2.annotations.BeansSource;
import ru.swayfarer.iocv2.context.Contexts;
import ru.swayfarer.iocv2.exception.IoCException;
import ru.swayfarer.swl2.asm.AsmUtils;
import ru.swayfarer.swl2.asm.informated.ClassInfo;
import ru.swayfarer.swl2.classes.ReflectionUtils;
import ru.swayfarer.swl2.exceptions.ExceptionsUtils;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1NoR;
import ru.swayfarer.swl2.z.dependencies.org.objectweb.asm.Type;

/**
 * Сканнер источников контекста <br>
 * 
 * @author swayfarer
 *
 */
public class SourcesScan implements IFunction1NoR<ClassInfo> {

	public static String desc = Type.getDescriptor(BeansSource.class);
	
	public void scan(ClassInfo classInfo)
	{
		var annotation = AsmUtils.findAnnotationRec(classInfo, desc);
		
		if (annotation != null)
		{
			var sourceClass = ReflectionUtils.findClass(classInfo.getCanonicalName());
			var sourceInstance = ReflectionUtils.newInstanceOf(sourceClass);
			
			ExceptionsUtils.IfNull(sourceClass, IoCException.class, "Can't create new instance of source", classInfo.getCanonicalName(), "! Maybe constructor without args is not exists?");
			
			var context = Contexts.findOrCreateContext(sourceClass);
			
			context.getContextManager().addContextSource(sourceInstance);
		}
	}

	@Override
	public void applyNoR(ClassInfo classInfo) 
	{
		scan(classInfo);
	}
	
}
