package ru.swayfarer.iocv2.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Иньекция поля из контекста <br> 
 * Отмечененное этой аннотацией поле будет заиньекчено из контекста <br> 
 * @author swayfarer
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Injection {
	
	
}
