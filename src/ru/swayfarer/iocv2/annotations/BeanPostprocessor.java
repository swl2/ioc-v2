package ru.swayfarer.iocv2.annotations;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Отмеченный этой аннотацией метод становится PostProcessor'ом для бинов контекста, в источнике которого расположен метод <br>
 * Постпроцессоры должны быть нестатическими и принимать в агрументы один {@link Object}. В них передаются все элементы, создаваемые в контексте. <br> 
 * Если метод возвращает {@link Object}, то возможна подмена обрабатываемого значения. Если метод void, то доступна только обработка <br> 
 * @author swayfarer
 *
 */
@Retention(RUNTIME)
@Target(METHOD)
public @interface BeanPostprocessor {}
