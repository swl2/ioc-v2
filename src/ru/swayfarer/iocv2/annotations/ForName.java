package ru.swayfarer.iocv2.annotations;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

import ru.swayfarer.swl2.classes.ReflectionUtils;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1;

/**
 * Явно указать имя бина <br>
 * Используется в комплекте с другими IoC-аннотациями для коррекции их работы 
 * <h1> При иньекции полей пустое значение будет означать иньекцию по типу! </h1>
 * @author swayfarer
 *
 */
@Retention(RUNTIME)
public @interface ForName {
	
	/** Имя бина */
	public String value();
	
	public static IFunction1<Method, String> methodToBeanNameFun = (field) -> {
		
		ForName beanNameAnnotation = ReflectionUtils.findAnnotationRec(field, ForName.class);
		
		if (beanNameAnnotation != null)
		{
			return beanNameAnnotation.value();
		}
		
		return null;
	};
	
	public static IFunction1<Class<?>, String> classToBeanNameFun = (field) -> {
		
		ForName beanNameAnnotation = ReflectionUtils.findAnnotationRec(field, ForName.class);
		
		if (beanNameAnnotation != null)
		{
			return beanNameAnnotation.value();
		}
		
		return null;
	};
	
	public static IFunction1<Field, String> fieldToBeanNameFun = (field) -> {
		
		ForName beanNameAnnotation = ReflectionUtils.findAnnotationRec(field, ForName.class);
		
		if (beanNameAnnotation != null)
		{
			return beanNameAnnotation.value();
		}
		
		return null;
	};
	
	public static IFunction1<Parameter, String> paramToBeanNameFun = (param) -> {
		
		ForName beanNameAnnotation = ReflectionUtils.findAnnotationRec(param, ForName.class);
		
		if (beanNameAnnotation != null)
		{
			return beanNameAnnotation.value();
		}
		
		return null;
	};
}
