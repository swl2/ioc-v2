package ru.swayfarer.iocv2.annotations;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.List;
import java.util.Map;

/**
 * В отмеченное этой аннотацией поле будут заиньекчены все бины, подходящие по типу. <br> 
 * Поле должно быть типа {@link List} или {@link Map}. В первом случае в лист будут собраны все бины, подходящие по generic-типу. 
 * Во втором то же самое, но ключами будут имена бинов.
 * @author swayfarer
 *
 */
@Retention(RUNTIME)
@Target(FIELD)
public @interface FindBeans {}
