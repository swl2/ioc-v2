package ru.swayfarer.iocv2.annotations;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import ru.swayfarer.iocv2.componentscan.AutoComponentScan;

/**
 * Маркер для {@link AutoComponentScan}, отвечающий за настройку сканирования <br> 
 * Автоматическое сканирование самостоятельно найдет составляющие контекстов и зарегистрирует их.
 * 
 * @author swayfarer
 *
 */
@Retention(RUNTIME)
@Target(TYPE)
public @interface EnableAutoScan {

	/** Сканировать ли бины? (классы, отмеченные {@link Bean}) */
	public boolean beans() default true;
	
	/** Сканировать ли источники? (классы, отмеченные {@link BeansSource}) */
	public boolean sources() default true;
	
	/** Контекст с этим именем будет автоматически зарегистрирован для всех сканируемых пакетов */
	public String context() default "";
	
	/** 
	 * Сканируемые пакеты <br>
	 * Пакет, в котором находится отмеченный класс, сканируется в любом случае <br>
	 */
	public String[] packages() default {""};
	
}
