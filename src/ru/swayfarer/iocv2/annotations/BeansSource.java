package ru.swayfarer.iocv2.annotations;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import ru.swayfarer.iocv2.componentscan.SourcesScan;

/**
 * Отмеченный этой аннотацией класс подхватывается {@link SourcesScan} как источник контекста, 
 * который определяется в зависимости от окружения, например аннотации {@link ForContext} или паттернов контекста. <br> 
 * Если источник отмечен аннотацией {@link ForName}, а контекста не будет зарегистировано, то это произойдет автоматичеки <br> 
 * Источники могут содержать постробработчики бинов (см {@link BeanPostprocessor}) и их источники (см {@link Bean}) 
 * @author swayfarer
 *
 */
@Retention(RUNTIME)
@Target(TYPE)
public @interface BeansSource {

	@Retention(RUNTIME)
	public static @interface PreInit {
		
	}
	
}
