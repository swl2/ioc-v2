package ru.swayfarer.iocv2.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import ru.swayfarer.iocv2.bean.BeanType;

/**
 * Отмеченный этой аннотацией элемент (метод, класс) становится бином <br>
 * Контекст бина определяется окружением, вроде наличия аннотации {@link ForName}, или паттерна контекста <br> 
 * Указывает тип бина
 * @author swayfarer
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Bean {
	
	/** Тип бина */
	public BeanType type() default BeanType.Singleton;
}
