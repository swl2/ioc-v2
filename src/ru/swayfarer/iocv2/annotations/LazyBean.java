package ru.swayfarer.iocv2.annotations;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Явно задать "Ленивость" бина <br> 
 * По-умолчанию бин ленивый. Ленивые бины инициализируются при первом обращении к ним. <br> 
 * Не-ленивые бины инициализируются автоматичеки.
 * Используется в комплекте с другими IoC-аннотациями для коррекции их работы 
 * @author swayfarer
 *
 */
@Retention(RUNTIME)
@Target(FIELD)
public @interface LazyBean {

	/** True, если бин ленивый */
	public boolean value();
	
}
