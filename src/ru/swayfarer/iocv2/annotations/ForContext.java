package ru.swayfarer.iocv2.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

import ru.swayfarer.iocv2.exception.IoCException;
import ru.swayfarer.swl2.classes.ReflectionUtils;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1;
import ru.swayfarer.swl2.markers.InternalElement;

/**
 * Аннотация, явно указывающая контекст. <br>
 * Используется в комплекте с другими IoC-аннотациями для коррекции их работы 
 * @author swayfarer
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface ForContext {

	/** Имя контекста. При указании пустого имени будет {@link IoCException} */
	public String value();
	
	@InternalElement
	public static final IFunction1<Parameter, String> contextNameByAnnotationOfParamFun = (e) -> {
		
		ForContext forContextAnnotation = ReflectionUtils.findAnnotationRec(e, ForContext.class);
		
		if (forContextAnnotation != null)
		{
			return forContextAnnotation.value();
		}
		
		return null;
	};
	
	@InternalElement
	public static final IFunction1<Class<?>, String> contextNameByAnnotationFun = (e) -> {
		
		ForContext forContextAnnotation = ReflectionUtils.findAnnotationRec(e, ForContext.class);
		
		if (forContextAnnotation != null)
		{
			return forContextAnnotation.value();
		}
		
		return null;
	};
	
	@InternalElement
	public static final IFunction1<Field, String> contextNameByAnnotationOfFieldFun = (e) -> {
		
		ForContext forContextAnnotation = ReflectionUtils.findAnnotationRec(e, ForContext.class);
		
		if (forContextAnnotation != null)
		{
			return forContextAnnotation.value();
		}
		
		return null;
	};
	
	@InternalElement
	public static final IFunction1<Method, String> contextNameByAnnotationOfMethodFun = (e) -> {
		
		ForContext forContextAnnotation = ReflectionUtils.findAnnotationRec(e, ForContext.class);
		
		if (forContextAnnotation != null)
		{
			return forContextAnnotation.value();
		}
		
		return null;
	};
}
