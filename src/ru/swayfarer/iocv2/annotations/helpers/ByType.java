package ru.swayfarer.iocv2.annotations.helpers;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

import ru.swayfarer.iocv2.annotations.ForName;

/**
 * Отмеченное этой аннотацией поле будет игнорировать имена бинов при иньекции 
 * @author swayfarer
 *
 */
@ForName("")
@Retention(RUNTIME)
public @interface ByType {}
