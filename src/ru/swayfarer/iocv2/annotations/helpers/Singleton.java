package ru.swayfarer.iocv2.annotations.helpers;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import ru.swayfarer.iocv2.annotations.Bean;
import ru.swayfarer.iocv2.bean.BeanType;

/**
 * {@link Bean} типа {@link BeanType#Singleton}
 * @author swayfarer
 *
 */
@Bean(type = BeanType.Singleton)
@Retention(RetentionPolicy.RUNTIME)
public @interface Singleton {

}
