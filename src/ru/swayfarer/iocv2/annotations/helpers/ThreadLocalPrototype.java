package ru.swayfarer.iocv2.annotations.helpers;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

import ru.swayfarer.iocv2.annotations.Bean;
import ru.swayfarer.iocv2.bean.BeanType;

/**
 * {@link Bean} типа {@link BeanType#ThreadLocalPrototype}
 * @author swayfarer
 *
 */
@Bean(type = BeanType.ThreadLocalPrototype)
@Retention(RUNTIME)
public @interface ThreadLocalPrototype {

}
