package ru.swayfarer.iocv2.annotations.helpers;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

import ru.swayfarer.iocv2.annotations.Injection;

/**
 * Аналог {@link Injection}, но с игнорированием имен бинов
 * @author swayfarer
 *
 */
@Injection
@ByType
@Retention(RUNTIME)
public @interface InjectionByType {

}
