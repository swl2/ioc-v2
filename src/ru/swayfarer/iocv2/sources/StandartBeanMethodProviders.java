package ru.swayfarer.iocv2.sources;

import ru.swayfarer.iocv2.bean.ContextBean;
import ru.swayfarer.iocv2.bean.BeanType;
import ru.swayfarer.iocv2.bean.params.BeanParams;
import ru.swayfarer.iocv2.sources.SourcesHelper.BeanCreationEvent;
import ru.swayfarer.iocv2.sources.SourcesHelper.ContextBeanMethodProvider;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.exception.ExceptionHandler;

@SuppressWarnings("unchecked")
public interface StandartBeanMethodProviders {

	public static final ContextBeanMethodProvider singletonProvider = new SingletonMethodBeanProvider()
			.registerType(BeanType.Singleton)
	;
	
	public static final ContextBeanMethodProvider threadLocalSingletonProvider = new ThreadLocalSingletonMethodBeanProvider()
			.registerType(BeanType.ThreadLocalPrototype)
	;
	
	public static final ContextBeanMethodProvider prototypeProvider = new MethodBeanProvider()
			.registerType(BeanType.Prototype)
	;
	
	public static class SingletonMethodBeanProvider extends MethodBeanProvider {
		
		@Override
		public ContextBean apply(BeanCreationEvent evt) {
			var ret = super.apply(evt);
			
			if (ret == null)
				return null;
			
			ret.valueCreationFun = ret.valueCreationFun.memorized();
			return ret;
		}
		
	}
	
	public static class ThreadLocalSingletonMethodBeanProvider extends MethodBeanProvider {
		
		@Override
		public ContextBean apply(BeanCreationEvent evt) {
			var ret = super.apply(evt);
			
			if (ret == null)
				return null;
			
			ret.valueCreationFun = ret.valueCreationFun.threadLocal();
			
			return ret;
		}
	}
	
	public static class MethodBeanProvider implements ContextBeanMethodProvider {

		public IExtendedList<BeanType> acceptingTypes = CollectionsSWL.createExtendedList();
		
		public ExceptionHandler exceptionsHandler = new ExceptionHandler()
				.configure()
					.rule()
						.throwEx()
		.toHandler();
		
		public <T extends MethodBeanProvider> T registerType(BeanType beanType)
		{
			this.acceptingTypes.add(beanType);
			return (T) this;
		}

		
		@Override
		public ContextBean apply(BeanCreationEvent evt) {
			
			var type = evt.getBeanType();
			
			if (!acceptingTypes.contains(type))
			{
				return null;
			}
			
			var context = evt.getSourcesHelper().getContext();
			var method = evt.getMethod();
			
			var bean = ContextBean.builder()
					
					.beanParams(BeanParams.builder()
							.isSingleton(type == BeanType.Singleton)
							.isLazy(evt.isLazy())
							.name(evt.getBeanName())
							.type(method.getReturnType())
							.beanType(type)
							.context(context)
						.build())
					
					.valueCreationFun(() -> {
						return exceptionsHandler.safeReturn(() -> {
							var methodArgs = context.getContextManager().findParamsFromContext(method, method.getParameters());
							return methodArgs == null ? null : method.invoke(evt.getSourcesHelper().getSourceInstance(), methodArgs.toArray());
						}, null, "Error while creating bean from method", method);
					})
					
			.build();
			
			return bean;
		}
	}
}
