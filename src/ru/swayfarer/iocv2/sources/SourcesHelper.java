package ru.swayfarer.iocv2.sources;

import java.lang.reflect.Method;

import lombok.Builder;
import lombok.Data;
import ru.swayfarer.iocv2.annotations.Bean;
import ru.swayfarer.iocv2.annotations.BeanPostprocessor;
import ru.swayfarer.iocv2.annotations.LazyBean;
import ru.swayfarer.iocv2.bean.BeanType;
import ru.swayfarer.iocv2.bean.ContextBean;
import ru.swayfarer.iocv2.context.Context;
import ru.swayfarer.iocv2.context.postprocess.BeanPostprocessing;
import ru.swayfarer.iocv2.exception.IoCException;
import ru.swayfarer.swl2.classes.ReflectionUtils;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.exception.ExceptionHandler;
import ru.swayfarer.swl2.exceptions.ExceptionsUtils;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1;

@Data
public class SourcesHelper {

	public ExceptionHandler exceptionHandler = new ExceptionHandler();
	
	public IExtendedList<ContextBeanMethodProvider> beanCreationFuns = CollectionsSWL.createExtendedList();
	
	public Object sourceInstance;
	public Context context;
	
	public void readSource()
	{
		readBeans();
		readPostProcessors();
	}
	
	public void readPostProcessors()
	{
		var postProcessorMethods = ReflectionUtils.methods(sourceInstance)
				.nonStatics()
				.annotated(BeanPostprocessor.class)
				.args(Object.class)
				.returnType(void.class, Object.class)
		.toList();
		
		var postProcessors = BeanPostprocessing.findPostprocessors(context, true);
		
		for (var method : postProcessorMethods)
		{
			var retType = method.getReturnType();
			
			if (retType == void.class)
			{
				postProcessors.add((o) -> {
					
					exceptionHandler.safe(() -> {
						method.invoke(sourceInstance, o);
					}, "Error while invoking postprocessor method", method);
					
					return o;
				});
			}
			else
			{
				postProcessors.add((o) -> {
					return exceptionHandler.safeReturn(() -> {
						return method.invoke(sourceInstance, o);
					}, null, "Error while invoking postprocessor method", method);
				});
			}
		}
	}

	public void readBeans() 
	{
		var beanMethods = ReflectionUtils.methods(sourceInstance)
				.nonStatics()
				.filter((method) -> ReflectionUtils.findAnnotationRec(method, Bean.class) != null)
		.toList();
		
		for (var method : beanMethods)
		{
			Bean beanAnnotation = ReflectionUtils.findAnnotationRec(method, Bean.class);
			LazyBean lazyBeanAnnotaion = ReflectionUtils.findAnnotationRec(method, LazyBean.class);
			
			var configuration = context.getConfiguration();
			var event = BeanCreationEvent.builder()
					.sourcesHelper(this)
					.method(method)
					.beanType(beanAnnotation.type())
					.beanName(configuration.getBeanNameManager().getBeanName(method))
					.isLazy(lazyBeanAnnotaion != null ? lazyBeanAnnotaion.value() : configuration.isBeansLazyByDefault())
			.build();
			
			ContextBean bean = null;
			
			for (var fun : beanCreationFuns)
			{
				bean = fun.apply(event);
				
				if (bean != null)
					break;
			}
			
			ExceptionsUtils.IfNull(bean, IoCException.class, "Can't create bean from method", method);
			
			context.addBean(bean);
		}
	}
	
	@SuppressWarnings("unchecked")
	public <T extends SourcesHelper> T registerStandartMethodProviders()
	{
		beanCreationFuns.add(StandartBeanMethodProviders.singletonProvider);
		beanCreationFuns.add(StandartBeanMethodProviders.threadLocalSingletonProvider);
		beanCreationFuns.add(StandartBeanMethodProviders.prototypeProvider);
		
		return (T) this;
	}
	
	public static interface ContextBeanMethodProvider extends IFunction1<BeanCreationEvent, ContextBean> {}
	
	@Data
	@Builder
	public static class BeanCreationEvent {
		public SourcesHelper sourcesHelper;
		public Method method;
		public BeanType beanType;
		public String beanName;
		public boolean isLazy;
	}
}
