package ru.swayfarer.iocv2.context.beanhandlers;

import ru.swayfarer.iocv2.bean.BeanType;
import ru.swayfarer.iocv2.context.BeanAddingEvent;
import ru.swayfarer.iocv2.context.postprocess.BeanPostprocessing;
import ru.swayfarer.iocv2.context.postprocess.BeanPostprocessing.IBeanPostProcessingProvider;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction0;

public interface StandartBeanPostProcessingProviders {

	public BeanPostProcessingProvider singletonProvider = new SingletonBeanPostProcessingProvider();
	public BeanPostProcessingProvider threadLocalSingletonProvider = new ThreadLocalSingletonBeanPostProcessingProvider();
	public BeanPostProcessingProvider prototypeProvider = new PrototypeBeanPostProcessingProvider();
	
	public static class SingletonBeanPostProcessingProvider extends BeanPostProcessingProvider {
		public SingletonBeanPostProcessingProvider()
		{
			acceptingBeanTypes.add(BeanType.Singleton);
		}
		
		@Override
		public IFunction0<Object> getFun(BeanAddingEvent event) {
			return super.getFun(event).memorized();
		}
	}
	
	public static class ThreadLocalSingletonBeanPostProcessingProvider extends BeanPostProcessingProvider {
		public ThreadLocalSingletonBeanPostProcessingProvider()
		{
			acceptingBeanTypes.add(BeanType.ThreadLocalPrototype);
		}
		
		@Override
		public IFunction0<Object> getFun(BeanAddingEvent event) {
			
			return super.getFun(event).threadLocal();
		}
	}
	
	public static class PrototypeBeanPostProcessingProvider extends BeanPostProcessingProvider {
		public PrototypeBeanPostProcessingProvider()
		{
			acceptingBeanTypes.add(BeanType.Prototype);
		}
	}
	
	public static class BeanPostProcessingProvider implements IBeanPostProcessingProvider {

		public IExtendedList<BeanType> acceptingBeanTypes = CollectionsSWL.createExtendedList();
		
		@Override
		public IFunction0<Object> apply(BeanAddingEvent event)
		{
			var type = event.getBean().getBeanParams().getBeanType();
			
			if (acceptingBeanTypes.contains(type))
			{
				return getFun(event);
			}
			
			return null;
		}
		
		public IFunction0<Object> getFun(BeanAddingEvent event)
		{
			var originalCreationFun = event.getBean().getValueCreationFun();
			
			return originalCreationFun.andApply((o) -> {
				
				var postProcessors = BeanPostprocessing.findPostprocessors(event.getContext(), false);
				
				if (!CollectionsSWL.isNullOrEmpty(postProcessors))
				for (var fun : postProcessors)
				{
					o = fun.apply(o);
				}
				
				return o;
			});
		}
	}
}
