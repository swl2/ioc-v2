package ru.swayfarer.iocv2.context.beanhandlers;

import ru.swayfarer.iocv2.context.BeanAddingEvent;
import ru.swayfarer.iocv2.context.postprocess.BeanPostprocessing.IBeanPostProcessingProvider;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1NoR;

public class BeanPostProcessorHandler implements IFunction1NoR<BeanAddingEvent> {

	public IExtendedList<IBeanPostProcessingProvider> valueFunWrappers = CollectionsSWL.createExtendedList();
	
	@Override
	public void applyNoR(BeanAddingEvent event) 
	{
		var bean = event.getBean();
		
		for (var fun : valueFunWrappers)
		{
			var wrapped = fun.apply(event);
			
			if (wrapped != null)
			{
				bean.setValueCreationFun(wrapped);
				break;
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public <T extends BeanPostProcessorHandler> T registerDefaultProviders()
	{
		valueFunWrappers.add(StandartBeanPostProcessingProviders.singletonProvider);
		valueFunWrappers.add(StandartBeanPostProcessingProviders.threadLocalSingletonProvider);
		valueFunWrappers.add(StandartBeanPostProcessingProviders.prototypeProvider);
		
		return (T) this;
	}
}
