package ru.swayfarer.iocv2.context.beanhandlers;

import ru.swayfarer.iocv2.context.BeanAddingEvent;
import ru.swayfarer.iocv2.exception.IoCException;
import ru.swayfarer.swl2.exceptions.ExceptionsUtils;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1NoR;

public class EqualBeanNameHandler implements IFunction1NoR<BeanAddingEvent> {

	@Override
	public void applyNoR(BeanAddingEvent event)
	{
		var bean = event.getBean();
		var context = event.getContext();

		var newBeanName = bean.getBeanParams().getName();
		
		ExceptionsUtils.If (
				context.getBeans().contains((e) -> {
				var beanName = e.getBeanParams().getName();
				return beanName.equals(newBeanName);
			}), 
		IoCException.class, 
		"Bean with name '" + newBeanName + "' already exists!"
		);
	}

}
