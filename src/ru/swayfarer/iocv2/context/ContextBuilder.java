package ru.swayfarer.iocv2.context;

import ru.swayfarer.iocv2.componentscan.AutoComponentScan;
import ru.swayfarer.iocv2.exception.IoCException;
import ru.swayfarer.swl2.classes.ReflectionUtils;
import ru.swayfarer.swl2.exceptions.ExceptionsUtils;
import ru.swayfarer.swl2.string.StringUtils;

public class ContextBuilder {

	public Context context;
	
	public ContextBuilder(String name)
	{
		context = Contexts.findOrCreateContext(name);
	}
	
	public ContextBuilder scanAround(Object obj)
	{
		AutoComponentScan.scan(obj);
		return this;
	}
	
	public ContextBuilder pattern(String pattern)
	{
		Contexts.contextPatterns.registerPattern(context.getName(), pattern);
		return this;
	}
	
	public ContextBuilder pattern(String... patterns)
	{
		for (var pattern : patterns)
		{
			if (!StringUtils.isBlank(pattern))
				pattern(pattern);
		}
		
		return this;
	}
	
	public ContextBuilder forThisPackage()
	{
		return pattern(ExceptionsUtils.getPackageAt(1));
	}
	
	public ContextBuilder source(Object... sources)
	{
		var contextManager = context.getContextManager();
		
		for (var source : sources)
		{
			contextManager.addContextSource(source);
		}
		
		return this;
	}
	
	public ContextBuilder source(Class<?>... sources)
	{
		var contextManager = context.getContextManager();
		
		for (var source : sources)
		{
			var instance = ReflectionUtils.newInstanceOf(source);
			
			ExceptionsUtils.IfNull(instance, IoCException.class, "Can't create source from class", source, "maybe default constructor (without args) is not exists?");
			contextManager.addContextSource(source);
		}
		
		return this;
	}
	
	public Context build()
	{
		return context;
	}
}
