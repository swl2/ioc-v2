package ru.swayfarer.iocv2.context;

import lombok.Builder;
import lombok.Data;
import ru.swayfarer.iocv2.bean.BeanNameManager;

@Data
@Builder
public class ContextConfiguration {
	
	@Builder.Default
	public boolean isBeansLazyByDefault = true;
	
	public BeanNameManager beanNameManager;
}
