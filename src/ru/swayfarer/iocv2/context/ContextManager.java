package ru.swayfarer.iocv2.context;

import java.lang.reflect.Field;
import java.lang.reflect.Parameter;

import lombok.Builder;
import lombok.NonNull;
import ru.swayfarer.iocv2.annotations.ForName;
import ru.swayfarer.iocv2.bean.BeanNameManager;
import ru.swayfarer.iocv2.bean.ContextBean;
import ru.swayfarer.iocv2.exception.IoCException;
import ru.swayfarer.iocv2.sources.SourcesHelper;
import ru.swayfarer.loggingv2.LoggerV2;
import ru.swayfarer.loggingv2.manager.LoggingManagerV2;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.exceptions.ExceptionsUtils;
import ru.swayfarer.swl2.string.StringUtils;

@Builder
public class ContextManager {
	
	public static LoggerV2 logger = LoggingManagerV2.getLogger();
	
	@NonNull
	public Context context;
	
	public void addContextSource(Object instance)
	{
		var helper = new SourcesHelper().registerStandartMethodProviders();
		helper.context = context;
		helper.sourceInstance = instance;
		helper.readSource();
	}
	
	public Object findValueForField(Field field)
	{
		var bean = findBeanForField(field);
		
		if (bean == null)
			return null;
		
		return bean.getValue();
	}
	
	public ContextBean findBeanForField(Field field)
	{
		var name = context.getConfiguration().getBeanNameManager().getBeanName(field);
		
		return context.findBean(field.getType(), StringUtils.isEmpty(name) ? null : name);
	}
	
	/**
	 * Получить аргументы, которые будут переданы из контекста для вызова метода
	 * @param classOfMethod Возвращаемый тип метода
	 * @param params Информация о параметрах метода
	 * @return Лист аргументов
	 */
	public IExtendedList<Object> findParamsFromContext(Object sourceForLog, Parameter[] params)
	{
		return context.exceptionHandler
		.safeReturn(() -> {
			if (CollectionsSWL.isNullOrEmpty(params))
				return CollectionsSWL.createExtendedList();
			
			IExtendedList<Object> invokeArgs = CollectionsSWL.createExtendedList();
			
			for (Parameter param : params)
			{
				Class<?> classOfParam = param.getType();
				
				ExceptionsUtils.IfNot(param.isNamePresent(), IoCException.class, "Can't find name of parameter of", sourceForLog, "! Please, re-compile classes with -parameters flag!");
				
				var context = this.context;
				
				var name = ForName.paramToBeanNameFun.apply(param);
				
				if (name == null)
					name = param.getName();
				
				var contextElement = context.findBean(classOfParam, param.getName());
				
				ExceptionsUtils.IfNull(contextElement, IoCException.class, "Can't find bean for param", param, "of", sourceForLog);
				
				invokeArgs.add(contextElement.getValue());
			}
			
			return invokeArgs;
		}, null, "Error while getting params from context");
	}
}
