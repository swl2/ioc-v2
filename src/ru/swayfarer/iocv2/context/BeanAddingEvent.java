package ru.swayfarer.iocv2.context;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import ru.swayfarer.iocv2.bean.ContextBean;
import ru.swayfarer.swl2.observable.events.AbstractCancelableEvent;

@Data
@EqualsAndHashCode(callSuper = false)
@Builder
public class BeanAddingEvent extends AbstractCancelableEvent {

	@NonNull 
	public ContextBean bean;
	
	@NonNull
	public Context context;
	
}