package ru.swayfarer.iocv2.context.postprocess;

import java.util.Map;

import ru.swayfarer.iocv2.context.BeanAddingEvent;
import ru.swayfarer.iocv2.context.Context;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction0;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1;

public class BeanPostprocessing {
	
	public static volatile Map<String, IExtendedList<IFunction1<Object, Object>>> registeredPostProcessors = CollectionsSWL.createWeakMap();
	public static interface IBeanPostProcessingProvider extends IFunction1<BeanAddingEvent, IFunction0<Object>> {}

	public static IExtendedList<IFunction1<Object, Object>> findPostprocessors(Context context, boolean createIfNotFound)
	{
		return findPostprocessors(context.getName(), createIfNotFound);
	}
	
	public static IExtendedList<IFunction1<Object, Object>> findPostprocessors(String contextName, boolean createIfNotFound)
	{
		var ret = registeredPostProcessors.get(contextName);
		
		if (ret == null)
		{
			ret = CollectionsSWL.createExtendedList();
			registeredPostProcessors.put(contextName, ret);
		}
		
		return ret;
	}
}
