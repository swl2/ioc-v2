package ru.swayfarer.iocv2.context.dynamic;

import ru.swayfarer.iocv2.context.Contexts;
import ru.swayfarer.iocv2.exception.IoCException;
import ru.swayfarer.swl2.classes.ReflectionUtils;
import ru.swayfarer.swl2.exceptions.ExceptionsUtils;

public class DynamicContextsAdapter {

	/**
	 * Получить значение элемента контекста для поля объекта
	 * @param instance Объект, для поля которого ищется контекст
	 * @param fieldName Поле, для которого ищется контекст
	 * @return Значение или null, если не найдено (при этом значение тоже может быть null)
	 */
	public static <T> T getContextElementValue(Object instance, String fieldName)
	{
		var field = ReflectionUtils.findField(instance, fieldName);
		
		if (field != null)
		{
			var context = Contexts.findContext(instance.getClass(), field);
			
			ExceptionsUtils.IfNull(context, IoCException.class, "Can't find registered context for field! Please, register one to use ioc injections!", field);
		
			var bean = context.findBean(field.getType(), context.getConfiguration().getBeanNameManager().getBeanName(field));
			
			ExceptionsUtils.IfNull(bean, IoCException.class, "Can't find bean for field", field, "!");
			
			return bean.getValue();
		}
		
		return null;
	}
	
}
