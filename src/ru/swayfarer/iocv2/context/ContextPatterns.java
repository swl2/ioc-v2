package ru.swayfarer.iocv2.context;

import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedMap;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1;

@SuppressWarnings("unchecked")
public class ContextPatterns {

	public IExtendedMap<String, String> registeredPatterns = CollectionsSWL.createExtendedMap();
	
	public <T extends ContextPatterns> T registerPattern(String contextName, String pattern)
	{
		registeredPatterns.put(pattern, contextName);
		return (T) this;
	}
	
	public String getContextName(Class<?> cl)
	{
		String ret = null;
		int retLenght = -1;
		var className = cl.getName();
		
		for (var e : registeredPatterns)
		{
			var pattern = e.getKey();
			var name = e.getValue();
			
			if (className.startsWith(pattern))
			{
				var patternLenght = pattern.length();
				
				if (retLenght < patternLenght)
				{
					retLenght = patternLenght;
					ret = name;
				}
			}
		}
		
		return ret;
	}
	
	public IFunction1<Class<?>, String> getContextFun()
	{
		return this::getContextName;
	}
}
