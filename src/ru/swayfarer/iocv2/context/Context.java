package ru.swayfarer.iocv2.context;

import lombok.Data;
import lombok.NonNull;
import ru.swayfarer.iocv2.bean.BeanNameManager;
import ru.swayfarer.iocv2.bean.ContextBean;
import ru.swayfarer.iocv2.context.beanhandlers.BeanPostProcessorHandler;
import ru.swayfarer.iocv2.context.beanhandlers.EqualBeanNameHandler;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.exception.ExceptionHandler;
import ru.swayfarer.swl2.observable.IObservable;
import ru.swayfarer.swl2.observable.Observables;
import ru.swayfarer.swl2.string.StringUtils;

@Data
@SuppressWarnings("unchecked")
public class Context {

	public ContextManager contextManager = ContextManager.builder()
			.context(this)
	.build();
	
	public ContextConfiguration configuration = ContextConfiguration.builder()
			.beanNameManager(new BeanNameManager())
	.build();
	
	public IObservable<BeanAddingEvent> eventAddBean = Observables.createObservable();
	
	public ExceptionHandler exceptionHandler = new ExceptionHandler()
			.configure()
				.rule()
					.log()
	.toHandler();
	
	public IExtendedList<ContextBean> beans = CollectionsSWL.createExtendedList();
	
	@NonNull
	public String name;
	
	public <T extends Context> T addBean(ContextBean bean)
	{
		var event = BeanAddingEvent.builder()
				.bean(bean)
				.context(this)
		.build();
		
		eventAddBean.next(event);
		
		if (!event.isCanceled())
		{
			this.beans.add(bean);
		}
		
		return (T) this;
	}
	
	public ContextBean findBean(Class<?> type, String name)
	{
		ContextBean ret = null;
		
		for (var bean : beans)
		{
			var params = bean.getBeanParams();
			
			var beanName = params.getName();
			var beanType = params.getType();
			
			if (!StringUtils.isBlank(name) && !name.equals(beanName))
				continue;
			
			if (type == beanType)
			{
				ret = bean;
				break;
			}
			else if (type.isAssignableFrom(beanType))
			{
				ret = bean;
			}
		}
		
		return ret;
	}
	
	public <T extends Context> T registerDefaultHandlers()
	{
		eventAddBean.subscribe(new EqualBeanNameHandler());
		
		BeanPostProcessorHandler postProcessingHandler = new BeanPostProcessorHandler().registerDefaultProviders();
		eventAddBean.subscribe(postProcessingHandler);
		
		return (T) this;
	}
}
