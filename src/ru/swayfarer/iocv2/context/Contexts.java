package ru.swayfarer.iocv2.context;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

import ru.swayfarer.iocv2.annotations.ForContext;
import ru.swayfarer.iocv2.injection.ReflectionBeanInjector;
import ru.swayfarer.swl2.classes.ReflectionUtils;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1;
import ru.swayfarer.swl2.string.StringUtils;

public class Contexts {
	
	public static IExtendedList<IFunction1<Class<?>, String>> registeredContextGettingFuns = CollectionsSWL.createExtendedList();
	
	public static IExtendedList<Context> registeredContexts = CollectionsSWL.createExtendedList();
	
	public static ContextPatterns contextPatterns = new ContextPatterns();
	
	public static ContextBuilder context(String name)
	{
		return new ContextBuilder(name);
	}
	
	public static <T> T injectContextElements(T obj)
	{
		if (obj == null)
			return obj;
		
		var injector = ReflectionBeanInjector.of(obj.getClass());
		
		injector.inject(obj, null);
		
		return obj;
	}
	
	public static Context findOrCreateContext(String contextName)
	{
		var ret = findContextByName(contextName);
		
		if (ret == null)
		{
			ret = new Context(contextName).registerDefaultHandlers();
			registeredContexts.add(ret);
		}
		
		return ret;
	}
	
	public static Context createContextOfClass(Class<?> cl)
	{
		if (cl != null)
		{
			ForContext forContextAnnotation = ReflectionUtils.findAnnotationRec(cl, ForContext.class);
			
			if (forContextAnnotation != null)
			{
				return Contexts.findOrCreateContext(forContextAnnotation.value());
			}
		}
		
		return null;
	}
	
	public static Context findContext(Class<?> cl, Method method)
	{
		var contextName = ForContext.contextNameByAnnotationOfMethodFun.apply(method);
		
		if (StringUtils.isBlank(contextName))
		{
			return findContext(cl);
		}
		
		return findContextByName(contextName);
	}
	
	public static Context findContext(Class<?> cl, Field field)
	{
		var contextName = ForContext.contextNameByAnnotationOfFieldFun.apply(field);
		
		if (StringUtils.isBlank(contextName))
		{
			return findContext(cl);
		}
		
		return findContextByName(contextName);
	}
	
	public static Context findContext(Class<?> cl, Parameter param)
	{
		var contextName = ForContext.contextNameByAnnotationOfParamFun.apply(param);
		
		if (StringUtils.isBlank(contextName))
		{
			return findContext(cl);
		}
		
		return findContextByName(contextName);
	}
	
	public static Context findContext(Class<?> cl)
	{
		for (var fun : registeredContextGettingFuns)
		{
			var ret = fun.apply(cl);
			
			if (ret != null)
				return findContextByName(ret);
		}
		
		return null;
	}
	
	public static Context findOrCreateContext(Class<?> cl)
	{
		var ret = findContext(cl);
		
		return ret == null ? createContextOfClass(cl) : ret;
	}
	
	public static Context findContextByName(String name)
	{
		return registeredContexts.find((e) -> e.getName().equals(name));
	}
	
	public static void registerDefaultCreationFuns()
	{
		registeredContextGettingFuns.add(ForContext.contextNameByAnnotationFun);
		registeredContextGettingFuns.add(contextPatterns.getContextFun());
	}
	
	static
	{
		registerDefaultCreationFuns();
	}
}
