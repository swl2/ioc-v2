package ru.swayfarer.iocv2.injection;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Map;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv2.annotations.FindBeans;
import ru.swayfarer.iocv2.annotations.Injection;
import ru.swayfarer.iocv2.injection.handlers.BeanFinderHandler;
import ru.swayfarer.iocv2.injection.handlers.FieldInjectorHandler;
import ru.swayfarer.swl2.classes.ReflectionUtils;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.collections.extended.IExtendedMap;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1NoR;
import ru.swayfarer.swl2.observable.IObservable;
import ru.swayfarer.swl2.observable.Observables;

@SuppressWarnings("unchecked")
public class ReflectionBeanInjector {

	public static Map<Class<?>, ReflectionBeanInjector> classToInjector = CollectionsSWL.createIdentityMap();
	
	public static IExtendedMap<Class<? extends Annotation>, InjectionType> scanAnnotations = CollectionsSWL.createExtendedMap(
			Injection.class, InjectionType.Injection,
			FindBeans.class, InjectionType.Find
	);
	
	public IObservable<FieldInjectigEvent> eventInjection = Observables.createObservable();
	
	public IExtendedList<FieldCacheEntry> cachedFields = CollectionsSWL.createExtendedList();
	public IExtendedList<IFunction1NoR<FieldCacheEntry>> entryPostprocessors = CollectionsSWL.createExtendedList();
	
	public void inject(Object instance, InjectionSettings injectionSettings)
	{
		for (var fieldCacheEntry : cachedFields)
		{
			var event = FieldInjectigEvent.builder()
					.instance(instance)
					.fieldEntry(fieldCacheEntry)
					.type(fieldCacheEntry.getType())
					.injectionSettings(injectionSettings)
			.build();
			
			eventInjection.next(event);
		}
	}
	
	public void loadCache(Class<?> cl)
	{
		for (var field : ReflectionUtils.getAccessibleFields(cl).values())
		{
			for (var scanEntry : scanAnnotations)
			{
				var annotationClass = scanEntry.getKey();
				var annotation = ReflectionUtils.findAnnotationRec(field, annotationClass);
				
				if (annotation != null)
				{
					var cacheEntry = FieldCacheEntry.builder()
							.field(field)
							.annotation(annotation)
					.build();
					
					entryPostprocessors.each((e) -> e.apply(cacheEntry));
					
					cachedFields.add(cacheEntry);
				}
			}
		}
	}
	
	public static ReflectionBeanInjector of(Class<?> cl)
	{
		if (cl == null)
			return null;
		
		var cached = classToInjector.get(cl);
		
		if (cached != null)
			return cached;
		
		cached = new ReflectionBeanInjector();
		cached.registerDefaultHandlers();
		cached.loadCache(cl);
		
		classToInjector.put(cl, cached);
		
		return cached;
	}
	
	public <T extends ReflectionBeanInjector> T registerDefaultHandlers()
	{
		entryPostprocessors.add((e) -> {
			FindBeans beanAnnotation = ReflectionUtils.findAnnotationRec(e.getAnnotation(), FindBeans.class);
			if (beanAnnotation != null)
				e.setType(InjectionType.Find);
		});
		
		entryPostprocessors.add((e) -> {
			Injection beanAnnotation = ReflectionUtils.findAnnotationRec(e.getAnnotation(), Injection.class);
			if (beanAnnotation != null)
				e.setType(InjectionType.Injection);
		});
		
		var fieldsInjector = new FieldInjectorHandler().registerDefaultInjectors();
		eventInjection.subscribe(fieldsInjector);
		
		var beansFinder = new BeanFinderHandler().registerDefaultFoundInjectors();
		eventInjection.subscribe(beansFinder);
		
		return (T) this;
	}
	
	@Builder
	@Data
	@EqualsAndHashCode(callSuper = false)
	@Accessors(chain = true)
	public static class FieldInjectigEvent {
		public FieldCacheEntry fieldEntry;
		public Object instance;
		public InjectionType type;
		public InjectionSettings injectionSettings;
	}
	
	@Data
	@Builder
	public static class FieldCacheEntry {
		public Field field;
		public Annotation annotation;
		public InjectionType type;
	}
	
}
