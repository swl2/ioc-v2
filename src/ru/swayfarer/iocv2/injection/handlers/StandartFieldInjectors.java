package ru.swayfarer.iocv2.injection.handlers;

import ru.swayfarer.iocv2.injection.handlers.FieldInjectorHandler.IFieldInjector;
import ru.swayfarer.swl2.classes.ReflectionUtils;
import ru.swayfarer.swl2.exception.ExceptionHandler;
import ru.swayfarer.swl2.string.StringUtils;

public interface StandartFieldInjectors {
	
	public static final ExceptionHandler exceptionsHandler = new ExceptionHandler();
	
	public static final IFieldInjector fieldInjector = (field, value, instance) -> {
		return exceptionsHandler.safeReturn(() -> {
			field.setAccessible(true);
			field.set(instance, value);
			return true;
		}, false, "Error while injecting field", field, "by reflection");
	};
	
	public static final IFieldInjector setterInjector = (field, value, instance) -> {
		return exceptionsHandler.safeReturn(() -> {
			
			var method = ReflectionUtils.methods(instance)
				.argsCount(1)
				.named("set" + StringUtils.firstCharToUpperCase(field.getName()))
			.first();
			
			if (method != null)
			{
				method.invoke(instance, value);
				return true;
			}
			
			return false;
		}, false, "Error while injecting field", field, "by setter");
	};
}
