package ru.swayfarer.iocv2.injection.handlers;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.swayfarer.iocv2.bean.ContextBean;
import ru.swayfarer.iocv2.exception.IoCException;
import ru.swayfarer.iocv2.injection.handlers.BeanFinderHandler.IFoundInjector;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.collections.extended.IExtendedMap;
import ru.swayfarer.swl2.collections.observable.IObservableList;
import ru.swayfarer.swl2.collections.wrapper.MapWrapper;
import ru.swayfarer.swl2.exceptions.ExceptionsUtils;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1;
import ru.swayfarer.swl2.generics.GenericObject;

@SuppressWarnings({ "rawtypes", "unchecked" })
public interface StandartFoundInjectorFuns {
	
	public static ListFoundInjector listInjector = new ListFoundInjector().registerDefaultCreationFuns();
	
	public static MapFoundInjector mapInjector = new MapFoundInjector().registerDefaultCreationFuns();
	
	public static class ListFoundInjector implements IFoundInjector{

		public IExtendedList<IFunction1<Class<?>, List>> listCreationFuns = CollectionsSWL.createExtendedList();
		
		@Override
		public Boolean apply(Field field, Object instance, IExtendedList<ContextBean> beans)
		{
			var fieldType = field.getType();
			
			if (List.class.isAssignableFrom(fieldType))
			{
				var generics = GenericObject.ofField(field);
				
				ExceptionsUtils.If(generics == null || generics.size() != 1, IoCException.class, "Invalid generics at field", field, " It must be single genetic with bean type! (as List<ISomeThing>) ");
				
				var typeGeneric = generics.getFirstElement();
				var type = typeGeneric.loadClassSafe();
				
				ExceptionsUtils.IfNull(type, IoCException.class, "Can't find beans of non-loaded type:", type);
				
				try
				{
					List list = (List<?>) field.get(instance);
					
					if (list == null)
					{
						for (var fun : listCreationFuns)
						{
							list = fun.apply(fieldType);
							
							if (list != null)
							{
								field.set(instance, list);
								break;
							}
						}
					}
					
					ExceptionsUtils.IfNull(list, IoCException.class, "Can't store found beans in null list!");
					
					var beansList = beans.dataStream()
							.filter((e) -> e.getBeanParams().getType() != instance.getClass())
							.filter((e) -> type.isAssignableFrom(e.getBeanParams().getType()))
							.map((e) -> e.getValue())
					.toList();
					
					list.addAll(beansList);
					
					if (type.isAssignableFrom(instance.getClass()))
						list.add(instance);
				}
				catch (IoCException e)
				{
					throw e;
				}
				catch (Throwable e)
				{
					throw new IoCException("Error while injecting beans to field " + field, e);
				}
				
				return true;
			}
			
			return false;
		}
		
		public <T extends ListFoundInjector> T registerDefaultCreationFuns()
		{
			listCreationFuns.add((cl) -> {
				if (IObservableList.class.isAssignableFrom(cl))
					return CollectionsSWL.createObservableList();
				return null;
			});
			
			listCreationFuns.add((cl) -> {
				if (IExtendedList.class.isAssignableFrom(cl))
					return CollectionsSWL.createExtendedList();
				return null;
			});
			
			listCreationFuns.add((cl) -> {
				if (List.class.isAssignableFrom(cl))
					return new ArrayList<>();
				return null;
			});
			
			return (T) this;
		}
	}
	
	public static class MapFoundInjector implements IFoundInjector{
		
		public IExtendedList<IFunction1<Class<?>, Map>> mapCreationFuns = CollectionsSWL.createExtendedList();
		
		@Override
		public Boolean apply(Field field, Object instance, IExtendedList<ContextBean> beans)
		{
			var fieldType = field.getType();
			
			if (Map.class.isAssignableFrom(fieldType))
			{
				var generics = GenericObject.ofField(field);
				
				ExceptionsUtils.If(generics == null || generics.size() != 2, IoCException.class, "Invalid generics at field", field, " It must be two genetics with String & bean type! (as Map<String, ISomeThing>) ");
				
				var typeGeneric = generics.get(1);
				var type = typeGeneric.loadClassSafe();
				
				ExceptionsUtils.IfNull(type, IoCException.class, "Can't find beans of non-loaded type:", type);
				
				try
				{
					Map map = (Map) field.get(instance);
					
					if (map == null)
					{
						for (var fun : mapCreationFuns)
						{
							map = fun.apply(fieldType);
							
							if (map != null)
							{
								field.set(instance, map);
								break;
							}
						}
					}
					
					ExceptionsUtils.IfNull(map, IoCException.class, "Can't store found beans in null map!");
					
					var beansMap = beans.dataStream()
							.filter((e) -> type.isAssignableFrom(e.getBeanParams().getType()))
							.filter((e) -> e.getBeanParams().getType() != instance.getClass())
							.toMap((e) -> e.getBeanParams().getName(), (e) -> e.getValue())
					;
					
					map.putAll(beansMap);
				}
				catch (IoCException e)
				{
					throw e;
				}
				catch (Throwable e)
				{
					throw new IoCException("Error while injecting beans to field " + field, e);
				}
				
				return true;
			}
			
			return false;
		}
		
		public <T extends MapFoundInjector> T registerDefaultCreationFuns()
		{
			mapCreationFuns.add((cl) -> {
				if (IExtendedMap.class.isAssignableFrom(cl))
					return new MapWrapper<>(new HashMap<>());
				return null;
			});
			
			mapCreationFuns.add((cl) -> {
				if (Map.class.isAssignableFrom(cl))
					return new HashMap<>();
				return null;
			});
			
			return (T) this;
		}
	}
}
