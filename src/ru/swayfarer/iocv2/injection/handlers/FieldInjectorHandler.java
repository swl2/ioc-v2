package ru.swayfarer.iocv2.injection.handlers;

import java.lang.reflect.Field;

import ru.swayfarer.iocv2.context.Contexts;
import ru.swayfarer.iocv2.exception.IoCException;
import ru.swayfarer.iocv2.injection.InjectionType;
import ru.swayfarer.iocv2.injection.ReflectionBeanInjector.FieldInjectigEvent;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.exceptions.ExceptionsUtils;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1NoR;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction3;

@SuppressWarnings("unchecked")
public class FieldInjectorHandler implements IFunction1NoR<FieldInjectigEvent> {

	public IExtendedList<IFieldInjector> injectorFuns = CollectionsSWL.createExtendedList();
	
	public <T extends FieldInjectorHandler> T registerInjector(IFieldInjector fun)
	{
		this.injectorFuns.addExclusive(fun);
		return (T) this;
	}
	
	@Override
	public void applyNoR(FieldInjectigEvent event) 
	{
		var eventType = event.getType();
		
		if (eventType == InjectionType.Injection)
		{
			var instance = event.getInstance();
			var instanceClass = instance.getClass();
			var field = event.getFieldEntry().getField();
			var injectionSettings = event.getInjectionSettings();
			
			var context = Contexts.findContext(instanceClass, field);
			
			ExceptionsUtils.IfNull(context, IoCException.class, "Can't get context for field", field);
			
			var bean = context.getContextManager().findBeanForField(field);
			
			if (bean != null)
			{
				var value = bean.getValue();
				
				for (var fun : injectorFuns)
				{
					if (fun.apply(field, value, instance))
						break;
				}
			}
			else
			{
				if (injectionSettings == null || !injectionSettings.skipMissingBeans)
					throw new IoCException("Can't find bean for field " + field);
			}
		}
	}
	
	public <T extends FieldInjectorHandler> T registerDefaultInjectors()
	{
		registerInjector(StandartFieldInjectors.setterInjector);
		registerInjector(StandartFieldInjectors.fieldInjector);
		
		return (T) this;
	}
	
	public static interface IFieldInjector extends IFunction3<Field, Object, Object, Boolean> {};
}
