package ru.swayfarer.iocv2.injection.handlers;

import java.lang.reflect.Field;

import ru.swayfarer.iocv2.bean.ContextBean;
import ru.swayfarer.iocv2.context.Contexts;
import ru.swayfarer.iocv2.exception.IoCException;
import ru.swayfarer.iocv2.injection.InjectionType;
import ru.swayfarer.iocv2.injection.ReflectionBeanInjector.FieldInjectigEvent;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.exceptions.ExceptionsUtils;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1NoR;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction3;

@SuppressWarnings("unchecked")
public class BeanFinderHandler implements IFunction1NoR<FieldInjectigEvent> {

	public IExtendedList<IFoundInjector> foundInjectionFuns = CollectionsSWL.createExtendedList();
	
	@Override
	public void applyNoR(FieldInjectigEvent event) 
	{
		var type = event.getType();
		
		if (type != InjectionType.Find)
			return;
		
		var instance = event.getInstance();
		var field = event.getFieldEntry().getField();
		
		var context = Contexts.findContext(instance.getClass(), field);
		
		ExceptionsUtils.IfNull(context, IoCException.class, "Can't find context for field", field);
		
		var beans = context.getBeans();
		
		for (var fun : foundInjectionFuns)
		{
			if (fun.apply(field, instance, beans))
			{
				break;
			}
		}
	}
	
	public <T extends BeanFinderHandler> T registerFoundInjector(IFoundInjector injector)
	{
		this.foundInjectionFuns.addExclusive(injector);
		return (T) this;
	}
	
	public <T extends BeanFinderHandler> T registerDefaultFoundInjectors()
	{
		foundInjectionFuns.add(StandartFoundInjectorFuns.listInjector);
		foundInjectionFuns.add(StandartFoundInjectorFuns.mapInjector);
		
		return (T) this;
	}
	
	public static interface IFoundInjector extends IFunction3<Field, Object, IExtendedList<ContextBean>, Boolean> {}
}
