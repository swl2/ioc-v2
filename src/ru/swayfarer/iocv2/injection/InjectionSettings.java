package ru.swayfarer.iocv2.injection;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class InjectionSettings {

	@Builder.Default
	public boolean skipMissingBeans = true;
	
}
