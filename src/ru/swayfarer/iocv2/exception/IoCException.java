package ru.swayfarer.iocv2.exception;

@SuppressWarnings("serial")
public class IoCException extends RuntimeException {

	public IoCException() 
	{
		super();
	}

	public IoCException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) 
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public IoCException(String message, Throwable cause) 
	{
		super(message, cause);
	}

	public IoCException(String message) 
	{
		super(message);
	}

	public IoCException(Throwable cause) 
	{
		super(cause);
	}

}
